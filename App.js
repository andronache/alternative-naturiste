import React from 'react';
import { StackNavigator } from 'react-navigation';

const firebaseConfig = {
  apiKey: "AIzaSyDueDdRI-RssP4YqLKOnH677s-xjjI8aNo",
  authDomain: "reactnative-19e6f.firebaseapp.com",
  databaseURL: "https://reactnative-19e6f.firebaseio.com",
  projectId: "reactnative-19e6f",
  storageBucket: "reactnative-19e6f.appspot.com"
};
import * as firebase from 'firebase';

try {
  firebaseApp = firebase.initializeApp(firebaseConfig);
}
catch (err) {
  if (!/already exists/.test(err.message)) {
    console.error('Firebasdasdror ', err.stack);
  }
}


import Home from './src/routes/Home/Home';
import Overview from './src/routes/Overview/Overview';
import Products from './src/routes/Products/Products';
import Product from './src/routes/Product/Product';
import Webview from './src/routes/WebView/WebView';

import Colors from './src/assets/colors/color-palette';

const Nav = StackNavigator({
    Home: {
      screen: Home,
      navigationOptions: {
        title: 'App',
        headerStyle: {
          backgroundColor: Colors.DARKER_BLUE,
        },
        headerTintColor: Colors.WHITE
      }
    },
    Overview: {
      screen: Overview,
      navigationOptions: {
        title: 'Pachete',
        headerStyle: {
          backgroundColor: Colors.DARKER_BLUE,
        },
        headerTintColor: Colors.WHITE
      }
    },
    Products: {
      screen: Products,
      navigationOptions: {
        title: 'Produse',
        headerStyle: {
          backgroundColor: Colors.DARKER_BLUE,
        },
        headerTintColor: Colors.WHITE
      }
    },
    Product: {
      screen: Product,
      navigationOptions: {
        title: 'Produse',
        headerStyle: {
          backgroundColor: Colors.DARKER_BLUE,
        },
        headerTintColor: Colors.WHITE
      }
    },
    Webview: {
      screen: Webview,
      navigationOptions: {
        title: 'Cumpără',
        headerStyle: {
          backgroundColor: Colors.DARKER_BLUE,
        },
        headerTintColor: Colors.WHITE
      }
    }
  },
  {
    headerMode: 'screen',
    backgroundColor: Colors.DARKER_BLUE,
    headerTintColor: Colors.DARKER_BLUE,
    tintColor: Colors.DARKER_BLUE,
    headerStyle: {
      backgroundColor: Colors.DARKER_BLUE
    }
  });

console.disableYellowBox = true;

export default class App extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
        <Nav />
    );
  }
};
