import { StyleSheet, Dimensions } from 'react-native';
import Colors from './src/assets/colors/color-palette';

export default styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors.GRAY,
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
      padding: 0,
      margin: 0,
      width: '100%'
    },
    header: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'flex-start',
      minHeight: 50,
      maxHeight: 50,
      width: '100%',
      backgroundColor: Colors.DARKER_BLUE
    },
    headerText: {
      flex: 5,
      color: Colors.WHITE,
      fontSize: 18,
      fontWeight: 'bold',
      alignSelf: 'center',
      padding: 22
    },
    searchIconContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
    },
    searchIcon: {
      flex: 1,
      minWidth: 20,
      maxWidth: 20,
      minHeight: 20,
      maxHeight: 20,
      marginRight: 30,
      marginTop: 16
    },


    search: {
      flex: 1,
      flexDirection: 'row',
      width: '100%',
      alignSelf: 'center',
      justifyContent: 'center',
      marginTop: -5,
      minHeight: 50,
      maxHeight: 50,
      backgroundColor: Colors.DISEASE_BLUE,
      color: Colors.WHITE
    },
    textInput: {
      height: 50,
      width: '90%',
      textDecorationColor: Colors.WHITE,
      borderColor: Colors.WHITE,
      color: Colors.WHITE
    },


    notFound: {
      alignSelf: 'center',
      marginTop: 40
    },
    notFoundIcon: {
      alignSelf: 'center',
      marginBottom: 10,
      width: 40,
      height: 40
    },


    verticalScrollView: {
      width: '100%',
    },
    content: {
      flex: 1,
      width: '100%',
      height: '100%'
    },
    diseaseContainer: {
      flex: 5,
      width: '100%',
      alignSelf: 'center'
    },
    buttonsContainer: {
      flex: 1,
      flexDirection: 'row',
      width: '100%',
      minHeight: 50,
      maxHeight: 50,
    },
    productsButton: {
      flex: 1,
      justifyContent: 'flex-end',
    },
    packagesButton: {
      flex: 1,
      justifyContent: 'flex-end',
    },
    packagesButtonDefault: {
      flex: 1,
      alignSelf: 'center',
      backgroundColor: Colors.DEFAULT_BLUE,
      height: 50,
      justifyContent: 'center'
    },
    packagesButtonActive: {
      flex: 1,
      alignSelf: 'center',
      backgroundColor: Colors.BLACK,
      height: 50,
      justifyContent: 'center'
    },
    productsButtonDefault: {
      flex: 1,
      alignSelf: 'center',
      backgroundColor: Colors.DEFAULT_BLUE,
      height: 50,
      justifyContent: 'center'
    },
    productsButtonActive: {
      flex: 1,
      alignSelf: 'center',
      backgroundColor: Colors.BLACK,
      height: 50,
      justifyContent: 'center'
    },


    packagesContainer: {
      flex: 3,
      backgroundColor: Colors.BLACK,
      width: '100%',
      paddingTop: 22,
      paddingBottom: 22
    },
    itemPackage: {
      flex: 1,
      marginRight: 10,
      marginLeft: 0,
      width: Dimensions.get('window').width * 0.8
    },
    itemPackageLeft: {
      flex: 1,
      marginRight: 10,
      marginLeft: 12,
      width: Dimensions.get('window').width * 0.8
    },


    productsContainer: {
      flex: 1,
      width: '95%',
      alignSelf: 'center'
    },
    item: {
      flex: 1,
      width: '100%',
      maxHeight: 200,
      minHeight: 200,
      marginTop: 10,
    },


    modal: {
      width: '95%',
      maxHeight: Dimensions.get('window').height * 0.9,
      minHeight: Dimensions.get('window').height * 0.9,
      maxWidth: '95%',
      backgroundColor: Colors.WHITE,
      alignSelf: 'center',
      justifyContent: 'center',
      borderColor: 'rgba(0,0,0,0)',
      borderWidth: 1,
      borderRadius: 10
    },
    blanket: {
      height: '100%',
      width: '100%',
      minWidth: '100%',
      minHeight: '100%',
      backgroundColor: 'rgba(0, 0, 0, 0.8)',
      position: 'absolute'
    }
});