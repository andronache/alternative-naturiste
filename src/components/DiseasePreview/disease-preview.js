import React from 'react';
import {
  Text,
  TouchableHighlight,
  StyleSheet,
  View
} from 'react-native';

import Colors from '../../assets/colors/color-palette';

const DiseasePreview = (props) => {

  let productsNo = props.disease.data.length;

  return (
    <TouchableHighlight
      style={styles.container}
      onPress={() => props.navigate('Overview', {disease: props.disease, navigate: props.navigate})}
      >
      <View style={styles.subContainer}>
        <Text style={styles.name}>
          { props.disease.name }
        </Text>
        <Text style={styles.productsNo}>
          { productsNo } produse
        </Text>
      </View>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.DARKER_BLUE,
    minHeight: 80,
    maxHeight: 80,
    marginTop: 0,
    marginLeft: 2,
    marginRight: 2,
    marginBottom: 2,
  },

  subContainer: {
    flex: 1,
    width: '80%',
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: 3
  },

  name: {
    flex: 2,
    alignSelf: 'center',
    color: Colors.WHITE,
    fontSize: 20,
    justifyContent: 'center',
    textAlign: 'center',
    minHeight: 30,
    maxHeight: 30,
  },

  productsNo: {
    flex: 1,
    fontSize: 16,
    alignSelf: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    minHeight: 30,
    maxHeight: 30,
    color: Colors.GRAY
  }
});

export default DiseasePreview;