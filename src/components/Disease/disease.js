import React from 'react';
import { View, Text, Dimensions, ScrollView } from 'react-native';

import styles from './styles';

export default class Disease extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      showScrollView: false
    };
  }

  componentDidMount() {
    this.setState({
      name: this.props.name,
      description: this.props.description
    });
    // let text = this.props.description;
    // let width = Dimensions.get('window').width * 0.95;
    // let fontSize = 14;
    // console.log(text, width);
    // console.log('1');
    // const height = MeasureText.measure({
    //   text,
    //   width,
    //   fontSize
    // });
    // console.log('2');
    // const finalHeight = 5/3 * Dimensions.get('window').height - 100;
    // if (height.height > finalHeight) {
    //   this.state.showScrollView = true;
    //   this.setState(this.state);
    // }
    // console.log(this.state);
  }

  render() {
    return (
      <ScrollView
        style={styles.container}
        >
        <Text style={styles.name}>
          {this.state.name}
        </Text>
        <Text style={styles.description}>
          {this.state.description}
        </Text>
      </ScrollView>
    );
  }
};
