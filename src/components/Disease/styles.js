import { StyleSheet } from 'react-native';
import Colors from '../../assets/colors/color-palette';

export default styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: Colors.DISEASE_BLUE,
      paddingTop: 20
    },
    description: {
      flex: 1,
      paddingLeft: 20,
      paddingRight: 20,
      marginBottom: 20,
      color: Colors.WHITE,
      textAlign: 'justify',
      fontSize: 15
    },
    name: {
      paddingLeft: 20,
      paddingRight: 20,
      fontSize: 18,
      marginBottom: 12,
      color: Colors.WHITE
    }
});