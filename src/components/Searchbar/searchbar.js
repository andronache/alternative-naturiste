import React from 'react';
import {
  View,
  TextInput,
  Image,
  Text,
  StyleSheet
} from 'react-native';

import Colors from '../../assets/colors/color-palette';

export default class SearchBar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      notFound: this.props.notFound
    };
  }

  componentWillReceiveProps(nextProps) {
    this.state.notFound = nextProps.notFound;
    this.setState(this.state);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.search}>
          <Image
            src={require('../../assets/images/search.png')}
            style={styles.searchIcon}
            />
          <TextInput
            underlineColorAndroid={Colors.DARKER_BLUE}
            placeholderTextColor={Colors.BLACK}
            style={styles.textInput}
            onChangeText={(searchValue) => this.props.searchHandler(searchValue)}
            placeholder={'Ce cautam azi?'}
            blurOnSubmit={true}
            clearTextOnFocus={true}
            autoFocus={true}
          />
        </View>
        {
          this.state.notFound &&
          <View style={styles.notFound}>
            <Image
              source={require('../../assets/images/not-found.png')}
              style={styles.notFoundIcon}
            />
            <Text style={{alignSelf: 'center'}}>
              Nu s-a găsit nicio afecțiune bosule.
            </Text>
          </View>
        }
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    minHeight: 60,
    maxHeight: 60,
    alignSelf: 'flex-start',
    marginBottom: 2,
  },

  searchIconContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },

  searchIcon: {
    flex: 1,
    minWidth: 20,
    maxWidth: 20,
    minHeight: 20,
    maxHeight: 20,
    marginRight: 30,
    marginTop: 16
  },

  search: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
    minHeight: 60,
    maxHeight: 60,
    backgroundColor: Colors.WHITE,
  },
  textInput: {
    height: 57,
    width: '90%',
    marginTop: -2,
    textDecorationColor: Colors.BLACK,
    borderColor: Colors.BLACK,
    color: Colors.BLACK
  },

  notFound: {
    alignSelf: 'center',
    top: 50,
  },
  notFoundIcon: {
    alignSelf: 'center',
    marginBottom: 10,
    width: 40,
    height: 40
  },
});