import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';

import styles from './styles';

export default class Package extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: '',
      description: '',
      price: '',
      title: '',
      packageIndex: 0
    };
  }

  componentWillMount() {
    this.setState({
      image: this.props.source,
      description: this.props.description,
      price: this.props.price,
      title: this.props.title
    });
  }

  horizontalScroll = (direction) => {
    this.props.horizontalScroll(this.props.id, direction);
  };

  // swap = (direction) => {
  //   if(direction === 'right') {
  //     if(this.state.data[this.state.packageIndex] !== undefined) {
  //       let previous = {
  //         image: this.state.image,
  //         description: this.state.description,
  //         price: this.state.price,
  //         title: this.state.title
  //       };
  //       let price = this.state.data[this.state.packageIndex].price;
  //       let description = this.state.data[this.state.packageIndex].description;
  //       let image = this.state.data[this.state.packageIndex].source;
  //       let title = this.state.data[this.state.packageIndex].title;
  //       let packageIndex = this.state.packageIndex + 1;
  //       this.state.data[this.state.packageIndex] = previous;
  //       this.setState(
  //         {
  //           price: price,
  //           description: description,
  //           image: image,
  //           title: title,
  //           packageIndex: packageIndex
  //         }
  //       );
  //     }
  //     else {
  //       console.log("ho usor");
  //     }
  //     return;
  //   }
  //   if(this.state.data[this.state.packageIndex - 1] !== undefined) {
  //     let previous = {
  //       image: this.state.image,
  //       description: this.state.description,
  //       price: this.state.price,
  //       title: this.state.title
  //     };
  //     let price = this.state.data[this.state.packageIndex - 1].price;
  //     let description = this.state.data[this.state.packageIndex - 1].description;
  //     let image = this.state.data[this.state.packageIndex - 1].source;
  //     let title = this.state.data[this.state.packageIndex - 1].title;
  //     let packageIndex = this.state.packageIndex - 1;
  //     this.state.data[this.state.packageIndex - 1] = previous;
  //     this.setState(
  //       {
  //         price: price,
  //         description: description,
  //         image: image,
  //         title: title,
  //         packageIndex: packageIndex
  //       }
  //     );
  //   }
  //   else {
  //     console.log("stai asa");
  //   }
  // }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.left}>
          <View style={styles.leftInnerContainer}>
            <View style={styles.imageContainer}>
              <Image style={styles.image} source={require('../../assets/images/package1.jpg')} />
            </View>

            <View style={styles.price}>
              <Text style={styles.priceText}>
                ${ this.state.price }
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.right}>
          <Text style={styles.title}>
            { this.state.title }
          </Text>

          <View style={styles.descriptionContainer}>
            <Text style={styles.description}>
              { this.state.description.substring(0, 140) }
            </Text>
          </View>
        </View>

      </View>
    );
  }
};
