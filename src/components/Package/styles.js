import { StyleSheet } from 'react-native';
import Colors from '../../assets/colors/color-palette';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.WHITE,
    justifyContent: 'space-between',
    width: '100%',
  },

  title: {
    color: '#000',
    flex: 2,
    fontSize: 18,
    marginTop: 15,
    textAlign: 'center',
    alignSelf: 'center',
    top: 10,
  },

  descriptionContainer: {
    flex: 5,
    marginBottom: 20,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },

  description: {
    flex: 1,
    textAlign: 'center',
    justifyContent: 'center'
  },

  left: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 25,
    marginRight: 10,
    marginBottom: 8,
  },

  leftInnerContainer: {
    flex: 1,
    justifyContent: 'center',
    maxHeight: '70%',
  },

  price: {
    flex: 1,
    alignSelf: 'center',
    maxHeight: 25
  },

  priceText: {
    flex: 1,
    fontWeight: '200',
    fontSize: 20,
  },

  right: {
    flex: 3,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 20,
    marginLeft: 10,
    marginTop: -4,
  },

  imageContainer: {
    flex: 1,
    padding: 20,
    alignSelf: 'center',
    justifyContent: 'center'
  },

  image: {
    height: 100,
    width: 100,
    alignSelf: 'center',
    marginLeft: 0,
  }
});