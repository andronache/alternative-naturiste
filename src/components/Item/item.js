import React from 'react';
import { View, Text, Image, TouchableHighlight } from 'react-native';

import styles from './styles';

const item = props => {
  return (
    <View style={styles.container}>

      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          { props.title }
        </Text>
      </View>

      <View style={styles.body}>
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={{uri: props.source}} />
        </View>

        <View style={styles.rightColumn}>
          <View style={styles.descriptionContainer}>
            <Text style={styles.description}>
              { props.description.substring(0, 185) }...
            </Text>
          </View>
          <View style={styles.details}>
            <View style={styles.priceContainer}>
              <Text style={styles.price}>
                ${ props.price }
              </Text>
            </View>
            <TouchableHighlight
                style={styles.buyContainer}
                onPress={() => props.navigate('Webview', {url: props.url})}
                >
              <Text style={styles.buy}>
                Cumpără
              </Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>

    </View>
  );
};

export default item;
