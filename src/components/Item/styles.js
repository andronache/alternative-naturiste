import { StyleSheet } from 'react-native';
import Colors from '../../assets/colors/color-palette';

export default styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: Colors.WHITE,
      alignItems: 'center',
      width: '100%',
      paddingTop: 10,
      paddingBottom: 10,
      paddingRight: 13,
    },

    titleContainer: {
      flex: 1,
      height: 30,
      maxHeight: 30,
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
      alignContent: 'flex-start',
      alignSelf: 'flex-start',
      paddingLeft: 20,
    },
    title: {
      flex: 1,
      fontSize: 20,
      alignSelf: 'flex-start',
      color: Colors.BLACK,
      marginTop: 3,
    },


    body: {
      flex: 5,
      flexDirection: 'row',
      width: '100%'
    },
    imageContainer: {
      flex: 1,
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
      alignContent: 'flex-start',
      paddingTop: 10,
      marginRight: 5,
      marginLeft: 5
    },
    image: {
      flex: 1,
      alignSelf: 'flex-start',
      width: 100,
      height: 100,
      maxWidth: 100,
      maxHeight: 100,
      marginRight: 5,
      marginLeft: 5
    },


    rightColumn: {
      flex: 3,
      flexDirection: 'column',
      alignItems: 'center'
    },
    descriptionContainer: {
      flex: 3,
      alignItems: 'center',
      justifyContent: 'center',
      alignContent: 'center',
      paddingTop: 5,
      paddingBottom: 10,
      width: '90%',
      maxWidth: '90%'
    },
    description: {
      flex: 1,
      textAlign: 'left',
      justifyContent: 'center',
      alignSelf: 'center',
      alignContent: 'center',
      alignItems: 'center',
    },


    details: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      alignContent: 'center',
      paddingLeft: 10,
      height: 50,
      maxHeight: 50,
    },
    priceContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      alignContent: 'center',
      alignSelf: 'center',
      paddingRight: 15,
      height: '83%',
      maxHeight: '83%'
    },
    price: {
      flex: 1,
      fontSize: 19,
      textAlign: 'right',
      justifyContent: 'center',
      alignSelf: 'flex-end',
      alignContent: 'flex-end',
      paddingRight: 12,
      color: Colors.BLACK
    },


    buyContainer: {
      flex: 2,
      backgroundColor: Colors.DEFAULT_BLUE,
      width: 100,
      maxWidth: 100,
      padding: 7,
      borderWidth: 1,
      borderColor: Colors.DEFAULT_BLUE,
      borderRadius: 3,
    },
    buy: {
      color: Colors.WHITE,
      textAlign: 'center'
    }
});