
const Colors = {
  BLACK: '#203445',
  GRAY: '#e3e0e0',
  LIGHT_GRAY: '#ece8e8',
  DARK_GRAY: '#d2cfcf',
  DARKER_BLUE: '#2b587f',
  DEFAULT_BLUE: '#428bca',
  DISEASE_BLUE: '#3c6b94',
  WHITE: '#ffffff',
  RED: '#873131'
};

export default Colors;