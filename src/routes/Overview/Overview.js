import React from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';

import Package from '../../components/Package/package';
import Disease from '../../components/Disease/disease';

import Colors from '../../assets/colors/color-palette';
import styles from './styles';

export default class Overview extends React.Component {

  constructor(props) {
    super(props);
    this.state = props.navigation.state.params;
  }

  render() {
    return (
      <View style={styles.content}>

        <View style={styles.diseaseContainer}>

          <Disease
            name={this.state.disease.name}
            description={this.state.disease.description}
            />
          <View style={styles.delimiter}></View>
          <View style={styles.buttonsContainer}>
            <View style={styles.packagesButton}>
              <Text style={{alignSelf: 'center', color: Colors.WHITE, fontSize: 17}}>Pachete</Text>
            </View>

            <TouchableOpacity
              style={styles.productsButton}
              onPress={() => this.state.navigate('Products', {data: this.state.disease.data, navigate: this.state.navigate})}
            >
              <Text style={{alignSelf: 'center', color: Colors.WHITE, fontSize: 17}}>Produse</Text>

            </TouchableOpacity>
          </View>
        </View>

        {/* --------- PACKAGES VIEW --------- */}
          <View style={styles.packagesContainer}>
            <ScrollView
              horizontal={true}
              style={styles.horizontalScroll}
              showsHorizontalScrollIndicator={false}
              ref="scrollRef"
            >
              <TouchableHighlight
                elevation={5}
                style={styles.itemPackageLeft}
                onPress={() => this.state.navigate('Product', {
                  data: {
                    title: this.state.disease.package.title,
                    price: this.state.disease.package.price,
                    description: this.state.disease.package.description,
                    image: this.state.disease.package.image,
                    url: this.state.disease.package.url
                  },
                  navigate: this.state.navigate
                })}
              >
                <Package
                  source={this.state.disease.package.image}
                  description={this.state.disease.package.description}
                  price={this.state.disease.package.price}
                  title={this.state.disease.package.title}
                  left={false}
                  right={true}
                  horizontalScroll={(offset, direction) => this.horizontalScroll(offset, direction)}
                  id={0}
                />
              </TouchableHighlight>
              {
                this.state.disease.package.data.map((item, i) => {
                  return (
                    <TouchableHighlight
                      elevation={5}
                      style={styles.itemPackage}
                      key={i}
                      onPress={() => this.state.navigate('Product', {data: this.state.disease.package.data[i], navigate: this.state.navigate})}
                    >
                      <Package
                        source={item.image}
                        description={item.description}
                        price={item.price}
                        title={item.title}
                        right={this.state.disease.package.data[i + 1]}
                        left={true}
                        horizontalScroll={(offset, direction) => this.horizontalScroll(offset, direction)}
                        id={i+1}
                      />
                    </TouchableHighlight>
                  )
                })
              }
            </ScrollView>
          </View>
      </View>
    );
  }
}