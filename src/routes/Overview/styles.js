import React from 'react';
import {
  StyleSheet,
  Dimensions
} from 'react-native';
import Colors from '../../assets/colors/color-palette';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.GRAY,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    width: '100%',
    height: '100%'
  },
  diseaseContainer: {
    flex: 5,
    width: '100%',
    alignSelf: 'center'
  },
  buttonsContainer: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    minHeight: 50,
    maxHeight: 50,
  },
  delimiter: {
    minHeight: 10,
    maxHeight: 10,
    backgroundColor: Colors.DISEASE_BLUE
  },
  productsButton: {
    flex: 1,
    alignSelf: 'center',
    backgroundColor: Colors.DEFAULT_BLUE,
    height: 50,
    justifyContent: 'center'
  },
  packagesButton: {
    flex: 1,
    alignSelf: 'center',
    backgroundColor: Colors.BLACK,
    height: 50,
    justifyContent: 'center'
  },

  packagesContainer: {
    flex: 3,
    backgroundColor: Colors.BLACK,
    width: '100%',
    paddingTop: 22,
    paddingBottom: 22
  },
  itemPackage: {
    flex: 1,
    marginRight: 10,
    marginLeft: 0,
    width: Dimensions.get('window').width * 0.8
  },
  itemPackageLeft: {
    flex: 1,
    marginRight: 10,
    marginLeft: 12,
    width: Dimensions.get('window').width * 0.8
  },

  modal: {
    width: '95%',
    maxHeight: Dimensions.get('window').height * 0.9,
    minHeight: Dimensions.get('window').height * 0.9,
    maxWidth: '95%',
    backgroundColor: Colors.WHITE,
    alignSelf: 'center',
    justifyContent: 'center',
    borderColor: 'rgba(0,0,0,0)',
    borderWidth: 1,
    borderRadius: 10
  },
  blanket: {
    height: '100%',
    width: '100%',
    minWidth: '100%',
    minHeight: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    position: 'absolute'
  }
});