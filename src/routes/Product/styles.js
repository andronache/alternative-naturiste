import { StyleSheet } from 'react-native';
import Colors from '../../assets/colors/color-palette';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.WHITE,
    alignItems: 'flex-start',
    width: '100%',
  },

  header: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: 65,
    maxHeight: 65,
    paddingTop: 10
  },

  title: {
    color: '#000',
    flex: 1,
    fontSize: 26,
    alignSelf: 'center',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: 20
  },

  imageView: {
    flex: 2,
    width: '100%',
  },

  image: {
    height: 150,
    width: 150,
    alignSelf: 'center',
  },

  detailsContainer: {
    flex: 3,
    padding: 20
  },

  description: {
    fontSize: 18,
  },

  scrollView: {
    flex: 6
  },

  footer: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: 50,
    maxHeight: 50,
    alignSelf: 'center',
    marginTop: 20,
  },

  priceContainer: {
    flex: 1,
    backgroundColor: Colors.DARKER_BLUE,
    padding: 10,
    alignSelf: 'center',
    height: '100%',
    width: '100%'
  },

  price: {
    color: Colors.WHITE,
    height: '100%',
    width: '100%',
    textAlign: 'center',
    fontSize: 20,
  },

  redirectBg: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.DEFAULT_BLUE,
    height: '100%',
    width: '100%'
  },

  redirect: {
    height: 23,
    width: 23,
    maxHeight: 23,
    maxWidth: 23,
    alignSelf: 'center'
  }
});