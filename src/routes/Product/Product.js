import React from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  ScrollView,
  Image
} from 'react-native';

import styles from './styles';

export default class Product extends React.Component {

  constructor(props) {
    super(props);
    this.state = props.navigation.state.params;
  };

  // onScroll = (event) => {
  //   console.log('working')
  //   if(event.nativeEvent.contentOffset.y > 0) {
  //     this.state.minimizeHeader = true;
  //     console.log(1);
  //     this.setState(this.state);
  //     return;
  //   }
  //   if(event.nativeEvent.contentOffset.y === 0) {
  //     this.state.minimizeHeader = false;
  //     console.log(0);
  //     this.setState(this.state);
  //   }
  // };

  render() {
    return (
      <View style={styles.container}>

        <ScrollView
          showsVerticalScrollIndicator={false}
          scrollEventThrottle={16}
          onScroll={() => console.log("working")}
          style={styles.scrollView}
        >
          <View style={styles.header}>
            <Text style={styles.title}>
              { this.state.data.title }
            </Text>
          </View>

          <View style={styles.imageView}>
            <Image style={styles.image} source={{uri: this.state.data.image}} />
          </View>

          <View style={styles.detailsContainer}>

              <Text style={styles.description}>
                { this.state.data.description }
              </Text>

          </View>

        </ScrollView>

        <View style={styles.footer}>
          <View style={styles.priceContainer}>
            <Text style={styles.price}>
              ${ this.state.data.price }
            </Text>
          </View>
          <TouchableHighlight
            style={styles.redirectBg}
            onPress={() => this.state.navigate('Webview', {url: this.state.data.url})}
            >
            <Image
              source={require('../../assets/images/redirect.png')}
              style={styles.redirect}
            />
          </TouchableHighlight>
        </View>

      </View>
    );
  }
}