import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  ActivityIndicator
} from 'react-native';

import SearchBar from '../../components/Searchbar/searchbar';
import DiseasePreview from '../../components/DiseasePreview/disease-preview';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.itemsRef = this.getRef().child('diseases');
    this.state = {
      diseases: [
        {
          data: [],
          package: {
            description: '',
            image: '',
            price: '',
            title: '',
            data: []
          },
          tags: [],
          name: '',
          description: ''
        }
      ],
      modal: false,
      modalId: 1,
      searchBarDropped: true,
      notFound: false,
      diseasesFound: []
    }
  }

  componentWillMount() {
    this.getItems();
  }

  getRef = () => {
    return firebaseApp.database().ref();
  };

  getItems = () => {
    let diseases = [];
    new Promise((resolve) => {
      this.itemsRef.on('value', (snap) => {
        snap.forEach((disease) => {
          diseases.push(disease.val());
        });
        resolve(diseases);
      });
    }).then((diseases) => {
      this.setState({diseases});
    });
  };

  searchHandler = (searchValue) => {
    if(searchValue === '') {
      this.state.diseasesFound = [];
      this.state.notFound = true;
      this.setState(this.state);
      return;
    }
    let diseasesArray = [];
    let searchArray = searchValue.split(" ");
    this.state.diseases.forEach((disease) => {
      searchArray.forEach((keyword) => {
        let keywordPassed = false;
        disease.tags.forEach((tag) => {
          if(tag.name === keyword) {
            keywordPassed = true;
          }
        });
        if(keywordPassed) {
          diseasesArray.push(disease);
        }
      });
    });
    this.state.diseasesFound = [];
    if(diseasesArray.length) {
      diseasesArray.forEach((x) => {
        this.state.diseasesFound.push(x);
      });
      this.state.notFound = false;
      this.setState(this.state);
      return;
    }
    this.state.notFound = true;
    this.setState(this.state);
  };

  render() {

    const { navigate } = this.props.navigation;

    return(
      <View style={styles.container}>

        <SearchBar
          notFound={this.state.notFound}
          searchHandler={(searchValue) => this.searchHandler(searchValue)}
          />

        <ScrollView>
          {
            !this.state.diseases[0].data.length && <ActivityIndicator marginTop={100} size="large" color="#000" />
          }
          {
            this.state.diseasesFound.map((disease, i) => {
              return (
                <DiseasePreview
                  key={i}
                  disease={disease}
                  navigate={navigate}
                  />
              );
            })
          }
        </ScrollView>
      </View>
    );
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignContent: 'flex-start',
  },
  searchBar: {
    flex: 1,
    alignSelf: 'flex-start',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  }
});