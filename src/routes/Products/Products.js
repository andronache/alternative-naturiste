import React from 'react';
import {
  View,
  TouchableHighlight,
  ScrollView,
  TouchableOpacity,
  Text
} from 'react-native';

import styles from './styles';
import Colors from '../../assets/colors/color-palette';

import Item from '../../components/Item/item';

export default class Products extends React.Component {

  constructor(props) {
    super(props);
    this.state = props.navigation.state.params;
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.buttonsContainer}>
          <TouchableOpacity
            style={styles.packagesButton}
            onPress={() => this.props.navigation.goBack(null)}
            >
            <Text style={{alignSelf: 'center', color: Colors.WHITE, fontSize: 17}}>Pachete</Text>
          </TouchableOpacity>

          <View style={styles.productsButton}>
            <Text style={{alignSelf: 'center', color: Colors.WHITE, fontSize: 17}}>Produse</Text>
          </View>
        </View>

        <ScrollView
          style={styles.scrollView}
          showsVerticalScrollIndicator={false}
          >
          {
            this.state.data.map((item, i) => {
              return (
                <TouchableHighlight
                  elevation={5}
                  key={i}
                  style={styles.item}
                  onPress={() => this.state.navigate('Product', {data: this.state.data[i], navigate: this.state.navigate})}
                >
                  <Item
                    source={item.image}
                    title={item.title}
                    description={item.description}
                    price={item.price}
                    url={item.url}
                    navigate={this.state.navigate}
                  />
                </TouchableHighlight>
              );
            })
          }
        </ScrollView>
      </View>
    );
  }
}