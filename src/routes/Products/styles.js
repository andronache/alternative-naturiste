import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../assets/colors/color-palette';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.DARK_GRAY,
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollView: {
    flex: 1,
    width: '100%',
    alignSelf: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
  item: {
    flex: 1,
    width: '100%',
    maxHeight: 200,
    minHeight: 200,
    marginBottom: 4,
  },
  buttonsContainer: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    minHeight: 50,
    maxHeight: 50,
  },
  packagesButton: {
    flex: 1,
    alignSelf: 'center',
    backgroundColor: Colors.DEFAULT_BLUE,
    height: 50,
    justifyContent: 'center'
  },
  productsButton: {
    flex: 1,
    alignSelf: 'center',
    backgroundColor: Colors.BLACK,
    height: 50,
    justifyContent: 'center'
  },
  modal: {
    width: '95%',
    maxHeight: Dimensions.get('window').height * 0.9,
    minHeight: Dimensions.get('window').height * 0.9,
    maxWidth: '95%',
    backgroundColor: Colors.WHITE,
    alignSelf: 'center',
    justifyContent: 'center',
    borderColor: 'rgba(0,0,0,0)',
    borderWidth: 1,
    borderRadius: 10
  },
  blanket: {
    height: '100%',
    width: '100%',
    minWidth: '100%',
    minHeight: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    position: 'absolute'
  }
});