// import React from 'react';
// import * as firebase from 'firebase';
// import { Observable} from 'rxjs/Observable';
//
// const firebaseConfig = {
//   apiKey: "AIzaSyDueDdRI-RssP4YqLKOnH677s-xjjI8aNo",
//   authDomain: "reactnative-19e6f.firebaseapp.com",
//   databaseURL: "https://reactnative-19e6f.firebaseio.com",
//   projectId: "reactnative-19e6f",
//   storageBucket: "reactnative-19e6f.appspot.com"
// };
//
// const firebaseApp = firebase.initializeApp(firebaseConfig);
//
// export default class FirebaseService extends React.Component {
//   constructor(props) {
//     super(props);
//     this.itemsRef = this.getRef().child('data');
//     this.state = {
//       data: []
//     };
//     this.loadItems();
//     this.observer = {
//       next: function(value) {
//         console.log(value);
//       }
//     }
//     Rx.Observable.create(obs => {
//       obs.next('asd');
//     }).subscribe(observer);
//   }
//
//   test = () => {
//     console.log(this.observer);
//   }
//
//   getRef = () =>{
//     return firebaseApp.database().ref();
//   }
//
//   getItems = () => {
//     return this.state;
//   }
//
//   loadItems = () => {
//     this.itemsRef.on('value', (snap) => {
//       let data = [];
//       snap.forEach((child) => {
//         data.push({
//           description: child.val().description,
//           _key: child.key
//         });
//       });
//       this.setState(data);
//     });
//   }
// }
